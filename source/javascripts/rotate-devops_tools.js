(function() {
  var devops_toolsContainers = document.getElementsByClassName('devops-tools-container');

  function getLogos() {
    for (var i = 0; i < devops_toolsContainers.length; i++) {
      if (devops_toolsContainers[i].children.length > 1) {
        var tlLeft = new TimelineMax({ repeat: -1 });
        tlLeft.staggerTo(devops_toolsContainers[i].children, 0, {ease: Power4.easeOut, opacity: 1, zIndex: 1}, 4);
        tlLeft.staggerTo(devops_toolsContainers[i].children, 0, {ease: Power4.easeOut, opacity: 0, zIndex: 0}, 4);
        tlLeft.play();
      } else if (devops_toolsContainers[i].children.length) {
        devops_toolsContainers[i].firstElementChild.style.opacity = '1';
      }
    }
  }

  window.addEventListener('load', getLogos);
})();
